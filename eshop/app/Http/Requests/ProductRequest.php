<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /// condition for image upload on update
        $imageValidateRules = 'mimes:jpg,png|min:5|max:2048';
        if($this->isMethod('post')){
            $imageValidateRules = 'required|mimes:jpg,png|min:5|max:2048';
        }

        /// condition for title unique on update
        return [
            'title' => 'required|min:10|max:255|unique:products,title',
            'category_id' => 'required|exists:categories,id',
            'price' => 'required|numeric',
            'image' => $imageValidateRules
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Ekhane Title Obossoi Dite Hobe',
        ];
    }
}

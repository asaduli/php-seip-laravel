
<h1 style="text-align:center">Invoice</h1>
<p><b>Order No: </b>{{$order->order_no}}</p>
<p><b>Order By: </b>{{$order->user->name}}</p>
<p><b>Phone No: </b>{{$order->phone_no}}</p>
<p><b>Order Date:</b>{{$order->created_at}}</p>
<p><b>Payment Method: </b>{{$order->payment_method}}</p>
<p><b>Shipping Address: </b>{{$order->shipping_address}}</p>
<p><h6>Order Details:</h6></p>

<table>
<thead>
    <th>SL</th>
    <th>Item</th>
    <th>Qty</th>
    <th>Price</th>
</thead>
<tbody>
@php
$total_price=0;
@endphp
@foreach($order->details as $item)

<tr>
    <td>{{$loop->iteration}}</td>
    <td>{{$item->product_title}}</td>
    <td>{{$item->qty}}</td>
    <td>{{$item->qty*$item->unit_price}}</td>
    {{$total_price=($item->qty*$item->unit_price)+$total_price}}
</tr>
@endforeach
<tr>
    <td colspan="2"></td>
    <td><b>Total Price:</b></td>
    <td>{{$total_price}}</td>
</tr>

</tbody>


</table>
<br>
<br>
<i><h4>Thanks For Shoping with us</h4></i>


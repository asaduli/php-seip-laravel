
    <table border="1">
      <thead>
        <tr>
          <th scope="col">SL</th>
          <th scope="col">Order NO</th>
          <th scope="col">OrderBy</th>
          <th scope="col">Order Status</th>
          <th scope="col">Phone Number</th>
          <th scope="col">Email </th>
          <th scope="col">Shipping Address </th>
          <th scope="col">Payment Method </th>
        </tr>
      </thead>
      <tbody>
        @php
        $sl=1;
        @endphp
        @foreach($orders as $order)
        <tr>
          <td>{{$sl++}}</td>
          <td>{{$order->order_no}}</td>
          <td>{{$order->ordered_by}}</td>
          <td>{{$order->status}}</td>
          <td>{{$order->phone_no}}</td>
          <td>{{$order->email}}</td>
          <td>{{ Str::limit($order->shipping_address,30)}}</td>
          <td>{{$order->payment_method}}</td>
       
        @endforeach


      </tbody>
    </table>

 

<x-frontend.layouts.master>


    <div class="bg-light p-5 rounded">
        <div class="card">
            <div class="card-header"></div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <img src="{{ asset('storage/products/'.$product->image) }}" />
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <h3>Title: {{ $product->title }}</h3>
                        <p>Price: {{ $product->price }}</p>

                        <form method="post" action="{{ route('update-cart', $product->id) }}">
                            @csrf
                            <input type="number" name="qty" class="form-control">
                            <button type="submit" class="btn btn-primary">Add To Card</button>
                        </form>

                    </div>
                </div>
                
                <hr>
                
                <p>{!! $product->description !!}</p>
                
                <h1>Comments</h1>
               
                <ul>
                    @foreach ($product->comments as $comment)
                    <li> <b>{{ $comment->commentBy->name }}</b>
                        <p>{{ $comment->body }} </p>
                        <time><mark>{{ $comment->created_at->diffForHumans() }}</mark></time>
                    </li>
                    @endforeach
                </ul>

                @auth
                <form action="{{ route('product-comment', $product->id) }}" method="POST">
                    @csrf
                    <textarea rows="5" name="body" class="form-control"></textarea>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                @endauth

            </div>
        </div>
    </div>
</x-frontend.layouts.master>
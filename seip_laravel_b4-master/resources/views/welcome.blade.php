<x-frontend.layouts.master>

    <div class="bg-light p-5 rounded">
        <div class="row row-cols-1 row-cols-md-3 mb-3 text-center">

            @foreach ($products as $product)
            <div class="col">
                <div class="card mb-4 rounded-3 shadow-sm">
                    <div class="card-header">
                        <img src="{{ asset('storage/products/'.$product->image) }}" />
                    </div>
                    <div class="card-body">
                        <h4 class="card-title pricing-card-title">
                            <a href="{{ route('single-product', $product->id) }}"> {{ Str::limit($product->title, 50) }}</a>
                        </h4>
                        <p>TK</p>
                        <button type="button" class="w-100 btn btn-lg btn-outline-primary">Add To Cart</button>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        
    </div>
</x-frontend.layouts.master>
<?php

namespace App\Providers;

use App\Models\Category;
use App\View\Composers\FrontendComposer;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class FrontendServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
       // Using closure based composers...
       View::composer(['components.frontend.layouts.partials.header'], FrontendComposer::class);
    }
}

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Blade Templete Practice</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="Portal - Bootstrap 5 Admin Dashboard Template For Developers">
    <meta name="author" content="Xiaoying Riley at 3rd Wave Media">
    <link rel="shortcut icon" href="{{asset('backend/favicon.ico')}}">

    <!-- FontAwesome JS-->
    <script defer src="{{asset('assets/plugins/fontawesome/js/all.min.js')}}"></script>

    <!-- App CSS -->
    <link id="theme-style" rel="stylesheet" href="{{asset('backend/assets/css/portal.css')}}">

</head>

<body class="app">
    <header class="app-header fixed-top">
        
         <!-- Header-->
         <x-backend.layouts.partials.header>

         </x-backend.layouts.partials.header>
      
        <!--//app-header-inner-->
        <!-- sidebar -->
        <x-backend.layouts.partials.sidebar>

        </x-backend.layouts.partials.sidebar>
        <!--//app-sidepanel-->
    </header>
    <!--//app-header-->

    <d iv class="app-wrapper">

      <!--app content -->
       {{$slot}}
        <!--/app-content-->
          <!-- Footer-->
       <x-backend.layouts.partials.footer></x-backend.layouts.partials.footer>
        <!--//app-footer-->

    </d>
    <!--//app-wrapper-->


    <!-- Javascript -->
    <script src="{{asset('backend/assets/plugins/popper.min.js')}}"></script>
    <script src="{{asset('backend/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- Charts JS -->
    <script src="{{asset('backend/assets/plugins/chart.js/chart.min.js')}}"></script>
    <script src="{{asset('backend/assets/js/index-charts.js')}}"></script>

    <!-- Page Specific JS -->
    <script src="{{asset('backend/assets/js/app.js')}}"></script>

</body>

</html>
<x-backend.layouts.master>
  <a href="{{route('product.add')}}" class="btn btn-primary my-3 ms-5">Add Product</a>
  <div class="container-fluid">
    @if(Session::has('message'))
    <p class="alert alert-danger">{{session::get('message')}}</p>
    @endif
    <table class="table table-striped table-hover table-info">
      <thead>
        <tr>
          <th scope="col">SL</th>
          <th scope="col">Product Title</th>
          <th scope="col">Category</th>
          <th scope="col">Description</th>
          <th scope="col">Price</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        @php
        $sl=1;
        @endphp
        @foreach($products as $product)
        <tr>
          <td>{{$sl++}}</td>
          <td>{{$product->title}}</td>
          <td>{{$product->category->category_title??'N/A'}}</td>
          <td>{{Str::limit($product->description,30) }}</td>
          <td>{{$product->price}}</td>
          <td>
            <a class="btn btn-info" href="{{route('product.show',['id'=>$product->id])}}">Show</a>
            <a class="btn btn-primary" href="{{route('product.edit',['id'=>$product->id])}}">Edit</a>
           <form action="{{route('product.destroy',['id'=>$product->id])}}" method="post" style="display:inline">
          @csrf
          @method('DELETE')
          <button type="submit" class="btn btn-warning">Delete</button>
  
           </form></
            
          </td>
        </tr>
        @endforeach


      </tbody>
    </table>
  
    {{$products->links()}}
  </div>
</x-backend.layouts.master>
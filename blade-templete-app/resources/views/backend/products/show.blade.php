<x-backend.layouts.master>

<h5>Product/Show</h5>
<a href="{{route('admin.product')}}" class="btn btn-info btn">List</a>
<div class="card justify-self-center" style="width: 18rem;" >
  <img src="{{asset('storage/products/'.$product->image)}}" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">Title:{{$product->title}}</h5>
    <p class="card-text">Description:{{Str::limit($product->description,120)}}</p>
    <p> <b class="card-text">Price:{{$product->price}} Taka</b></p>
    <p> <b class="card-text">Category:{{$product->category->category_title??'N/A'}} </b></p>
    <a href="#" class="btn btn-primary">Add To Card</a>
  </div>
</div>
</x-backend.layouts.master>
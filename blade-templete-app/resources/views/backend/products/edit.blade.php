<x-backend.layouts.master>
<div class="container-fluid">
    <form action="{{route('product.update', ['id'=>$product->id])}}" method="post">
   @csrf
   @method('patch')
    
 <div class="mb-3 text-center">
  <label for="exampleFormControlInput1" class="form-label">Product Title</label>
  <input type="text" name="title" class="form-control" id="exampleFormControlInput1" value="{{$product->title}}">
</div>
<div class="mb-3 text-center">
  <label for="exampleFormControlTextarea1" class="form-label">Description</label>
  <textarea  name="description" class="form-control" id="exampleFormControlTextarea1" rows="3">{{$product->description}}</textarea>
</div>
<div class="mb-3 text-center">
  <label for="exampleFormControlTextarea1" class="form-label">Price</label>
   <input type="number" class="form-control" name="price" value="{{$product->price}}">
   
</div>
<div class="d-grid gap-2 col-4 mx-auto">
<button type="submit" class="btn btn-primary"> Update</button>
</div>
    </form>
    </div>

</x-backend.layouts.master>
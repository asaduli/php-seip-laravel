<x-backend.layouts.master> 
    <h5>Product/Add</h5>
<a href="{{route('admin.product')}}" class="btn btn-info btn col-md-1 mx-auto">List</a>
<div class="container-fluid">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <form action="{{route('product.store')}}" method="post" enctype="multipart/form-data">
   @csrf
    
 <div class="mb-3 text-center">
  <label for="exampleFormControlInput1" class="form-label">Product Title</label>
  <input type="text" name="title" class="form-control" id="exampleFormControlInput1" placeholder="Enter Product Title" value="{{old('title')}}">
  @error('title')
  <div class="text-danger">{{$message}}</div>
  @enderror 
</div>
   
<div class="mb-3 text-center">
  <label for="exampleFormControlInput1" class="form-label">Product Category</label>
 <select class="form-select" name="category_id" required>
     <option selected>Select A Category</option>
     @foreach($categories as $category)
<option value="{{$category->id}}" >{{$category->category_title}}</option>
  @endforeach
 </select>
  @error('category_id')
  <div class="text-danger">{{$message}}</div>
  @enderror
</div>
<div class="mb-3 text-center">
  <label for="exampleFormControlTextarea1" class="form-label">Description</label>
  <textarea  name="description" class="form-control" id="exampleFormControlTextarea1" rows="10">{{old('description')}}</textarea>
  @error('description')
  <div class="text-danger">{{$message}}</div>
  @enderror
</div>
<div class="mb-3 text-center">
  <label for="price" class="form-label">Price</label>
   <input type="number" class="form-control" name="price" value="{{old('price')}}" id="price">
   @error('price')
  <div class="text-danger">{{$message}}</div>
  @enderror
   
</div>
<div class="mb-3 text-center">
  <label for="image" class="form-label">Product Image:</label>
   <input type="file"  name="image" id="image" class="form-control">
   @error('image')
  <div class="text-danger">{{$message}}</div>
  @enderror
   
</div>
<div class="d-grid gap-2 col-4 mx-auto">
<button type="submit" class="btn btn-primary"> Add</button>
</div>

    </form>

    </div> 



</x-backend.layouts.master>
<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[FrontendController::class,'index'] 
)->name('home-page');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';



Route::prefix('admin')->middleware(['auth'])->group(function () {
    Route::get('/', function () {
        return view('backend.dashboard');
    })->name('admin');
    Route::get('/login', function () {
        return view('backend.login');
    })->name('admin.login');
    Route::get('/product', [ProductController::class, 'index'])
        ->name('admin.product');
    Route::get('/product/add',[CategoryController::class,'index']
    )->name('product.add');
    Route::post('/create', [ProductController::class, 'store'])->name('product.store');

    Route::get('/product/{id}', [ProductController::class, 'show'])->name('product.show');

    Route::get('/product/{id}/edit', [ProductController::class, 'edit'])->name('product.edit');

    Route::patch('/product/{id}', [ProductController::class, 'update'])->name('product.update');
    
    Route::delete('/product/{id}/destroy', [ProductController::class, 'destroy'])->name('product.destroy');

});


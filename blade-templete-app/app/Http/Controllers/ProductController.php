<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::paginate(20);
       return view('backend.products.index',compact('products'));
    }
    public function store(Request $request){

         
         $request->validate([
            'title' => 'required|min:10',
            'category_id'=>'required|numeric',
            'description' => 'required|max:500',
            'price' => 'required|numeric',
            'image'=>'required|mimes:jpg,png',
            
        ]);


       $path='public/products';
       $image=$request->file('image');
       $image_name=time().$image->getClientOriginalName();
        $request->file('image')->storeAs($path,$image_name);
        $data=Request()->all();
        $data['image']=$image_name;
        Product::create(
            [
                'title'=>$data['title'],
                'description'=>$data['description'],
                'price'=>$data['price'],
                'image'=>$data['image'],
                'category_id'=>$data['category_id'],
            ]
        );
        return redirect()->route('admin.product')->withMessage('Successfully Saved!');
    }
    public function show($id){

        $product=Product::findOrFail($id);
        
    
         return view('backend.products.show',compact('product'));
    }
    public function edit($id){

        $product=Product::findOrFail($id);
        return view('backend.products.edit',compact('product'));
    }
    public function update(Request $request, $id){
     $product=Product::findOrFail($id);
     //dd($request);
      $product->update([
          'title'=>$request->title,
          'description'=>$request->description,
          'price'=>$request->price
        
      ]); 
      return redirect()->route('admin.product')->withMessage("Successfully Updated");
    }
    public function destroy($id){

        $product=Product::findOrFail($id);
        $product->delete();
       return redirect()->route('admin.product')->withMessage("Successfully Delete");
    }

}

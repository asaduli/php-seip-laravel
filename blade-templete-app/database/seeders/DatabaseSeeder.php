<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Product::create(
        // [
        //  'title' => 'T_shirt',
        //  'description' => 'This is description',
        //  'price'=>'500'

        // ]

        // );
        $this->call([
            CategorySeeder::class,
        ]);
         \App\Models\Product::factory(100)->create();
    }
}
